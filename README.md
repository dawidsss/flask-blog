# Flask Blog (School Project)
This is my school project i made 2 years ago (in 2018). 

## Project requirements
- Can't use any ORM
- 6 Database Tables
- Every Table have to has endpoint for (Get, Create, Update, Delete)

## SETUP
1. Add `Secret_Key` to your environment variables  
2. Install requirements `pip install -r requirements.txt`  
3. RUN! `python app.py`  
4. Register User.  
**FIRST CREATED USER HAS AN ADMIN ROLE**
