import re


def is_email(email):
    EMAIL_REGEX = re.compile(r'[^@]+@[^@]+\.[^@]+')
    return EMAIL_REGEX.match(email)
