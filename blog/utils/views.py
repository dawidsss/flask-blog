from flask import views, request


class BaseView(views.MethodView):
    service = None

    def get_form(self):
        return  request.form

    def get_data(self):
        return request.get_json()

    def get_query(self):
        return request.args
