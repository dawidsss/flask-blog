import flask

from blog.apps.category import services


def get_session():
    flask.g.user = flask.session


def get_categories():
    service = services.CategoryService()
    flask.g.categories = service.get_all('category')
