import functools

import flask


def admin_required(decorated):
    @functools.wraps(decorated)
    def wrapper(*args, **kwargs):
        if flask.g.user is not None:
            if 'role_id' in flask.g.user:
                if flask.g.user['role_id'] == 1:
                    return decorated(*args, **kwargs)

        flask.flash('Wymagane uprawnienia admina')
        return flask.redirect(flask.url_for('blog.index_view'))

    return wrapper


def user_required(decorated):
    @functools.wraps(decorated)
    def wrapper(*args, **kwargs):
        if flask.g.user is not None:
            return decorated(*args, **kwargs)
        flask.flash('Wymagane logowanie')
        return flask.redirect(flask.url_for('blog.index_view'))

    return wrapper
