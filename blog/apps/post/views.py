from datetime import datetime

import flask

from blog.apps.post import services
from blog.apps.tag import services as tag_services
from blog.utils import views, decorators


class PostView(views.BaseView):
    service = services.PostService()

    def get(self, post_id):
        if post_id is not None:
            tag_service = tag_services.TagService()
            post = self.service.get_one('post', {'IdPost': post_id})
            tags = tag_service.get_tags(post_id)
            author = self.service.get_one('user', {'IdUser': post['idUser']})
            return flask.render_template('post.html', post=post, author_name=author['name'], tags=tags)
        else:
            flask.flash('Nie ma takiego postu')
            return flask.redirect(flask.url_for('blog.index_view'))


class CreatePostView(views.BaseView):
    service = services.PostService()

    @decorators.admin_required
    def get(self):
        return flask.render_template('createPost.html')

    @decorators.admin_required
    def post(self):
        data = self.get_form().to_dict()

        if 'title' not in data:
            flask.flash('Tytuł jest wymagany')
        if 'content' not in data:
            flask.flash('Treść postu jest wymagana')
        data['IdUser'] = flask.g.user['user_id']
        tags = data.pop('tags')
        data['Date'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        post_id = self.service.insert('post', data)
        if tags:
            tags = tags.replace(' ', '').split(',')
            self.service.add_tags(post_id, tags)

        flask.flash('Stworzono')
        return flask.redirect((flask.url_for('blog.index_view')))


class EditPostView(views.BaseView):
    service = services.PostService()

    def get(self, post_id):
        post = self.service.get_one('post', {'idPost': post_id})
        tags = self.service.select(f'SELECT t.name FROM  posttag pt, tag t WHERE pt.idPost == {post_id} AND pt.idTag == t.idTag')
        tags_names = ','.join(tag['name'] for tag in tags)
        return flask.render_template('editPost.html', post=post, tags=tags_names)

    def post(self, post_id):
        data = self.get_form().to_dict()

        if 'title' not in data:
            flask.flash('Tytuł jest wymagany')
        if 'content' not in data:
            flask.flash('Treść postu jest wymagana')

        tags = data.pop('tags')
        if tags:
            self.service.remove_tags(post_id)
            tags = tags.replace(' ', '').split(',')
            self.service.add_tags(post_id, tags)

        statement = (f'UPDATE post '
                     f'SET title = \"{data["title"]}\", content = '
                     f'\"{data["content"]}\", idCategory = {data["idCategory"]} '
                     f'WHERE idPost == {post_id} ')

        self.service.put(statement)
        return flask.redirect(flask.url_for('post.post_view', post_id=post_id))


class DeletePostView(views.BaseView):
    service = services.PostService()

    @decorators.admin_required
    def get(self, post_id):
        statement = f'DELETE FROM post WHERE idPost == {post_id}'
        self.service.delete(statement)
        self.service.remove_tags(post_id)
        flask.flash('Usunięto')
        return flask.redirect(flask.url_for('blog.index_view'))
