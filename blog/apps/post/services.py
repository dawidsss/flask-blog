from blog.apps.tag import services as tag_services
from blog.database import services


class PostService(services.Service):
    def get_latest_posts(self, count=5):
        return self.select((f'SELECT * FROM post '
                            f'ORDER BY Date DESC limit {count}'))

    def get_post(self, post_id):
        return self.select(
            (f'SELECT p.title, p.content, p.date, u.name FROM post p, user u '
             f'WHERE p.IdPost = {post_id} AND p.IdUser = u.IdUser'))

    def add_tag(self, post_id, tag_id):
        data = {'idPost': post_id, 'idTag': tag_id}
        return self.insert('PostTag', data)

    def add_tags(self, post_id, tags):
        tag_service = tag_services.TagService()
        for tag_name in tags:
            tag = self.get_one('tag', {'name': tag_name})
            if tag is None:
                tag = dict()
                tag['idTag'] = tag_service.new_tag(tag_name)
            self.add_tag(post_id, tag['idTag'])

    def remove_tags(self, post_id):
        statement = f'DELETE FROM posttag WHERE idPOST == {post_id}'
        self.delete(statement)
