import flask

from blog.apps.post import views

blueprint = flask.Blueprint('post', __name__)
blueprint.add_url_rule('/<post_id>', view_func=views.PostView.as_view('post_view'))
blueprint.add_url_rule('/new', view_func=views.CreatePostView.as_view('new_view'))
blueprint.add_url_rule('/<post_id>/edit', view_func=views.EditPostView.as_view('edit_view'))
blueprint.add_url_rule('/<post_id>/delete', view_func=views.DeletePostView.as_view('delete_view'))
