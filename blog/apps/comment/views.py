from blog.apps.comment import services
from blog.utils import views


class CommentView(views.BaseView):
    service = services.CommentService()

    def get(self, comment):
        pass

    def post(self):
        pass


class EditCommentView(views.BaseView):
    service = services.CommentService()

    # post = post, author_name = author['name'], tags = tags

    def get(self):
        pass

    def post(self):
        pass


class DeleteCommentView(views.BaseView):
    service = services.CommentService()

    def get(self):
        pass
