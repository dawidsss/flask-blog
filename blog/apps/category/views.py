import flask

from blog.apps.category import services
from blog.utils import views, decorators


class CategoryView(views.BaseView):
    service = services.CategoryService()

    def get(self, category_id):
        posts = self.service.get_latest_posts(category_id)
        category_name = self.service.get_category_name(category_id)
        if category_name:
            return flask.render_template('category.html', posts=posts, category_name=category_name)
        flask.flash('Nie ma takiej kategori')
        return flask.redirect(flask.url_for('blog.index_view'))


class CategoriesView(views.BaseView):
    service = services.CategoryService()

    @decorators.admin_required
    def get(self):
        return flask.render_template('categories.html')


class CreateCategoryView(views.BaseView):
    service = services.CategoryService()

    @decorators.admin_required
    def get(self):
        return flask.render_template('createCategory.html')

    @decorators.admin_required
    def post(self):
        data = self.get_form().to_dict()
        message = None

        if 'name' not in data:
            message = 'Nazwa jest wymagana'
        elif self.service.get_one('category', {'name': data['name']}):
            message = 'Kategoria juz istnieje'

        if message is None:
            self.service.insert('category', data)
            message = 'Stworzono'

        flask.flash(message)
        return flask.redirect(flask.url_for('category.categories_view'))


class EditCategoryView(views.BaseView):
    service = services.CategoryService()

    @decorators.admin_required
    def get(self, category_id):
        category = self.service.get_one('category', {'idCategory': category_id})
        return flask.render_template('editCategory.html', category=category)

    @decorators.admin_required
    def post(self, category_id):
        data = self.get_form().to_dict()
        message = None

        if 'name' not in data:
            message = 'Nazwa jest wymagana'
        elif self.service.get_one('category', {'name': data['name']}):
            message = 'Kategoria juz istnieje'

        if message is None:
            statement = (f'UPDATE category '
                         f'SET name = \"{data["name"]}\" '
                         f'WHERE idCategory == {category_id}')
            self.service.put(statement)
            message = 'Zedytowano'

        flask.flash(message)
        return flask.redirect(flask.url_for('category.categories_view'))


class DeletePostView(views.BaseView):
    service = services.CategoryService()

    @decorators.admin_required
    def get(self, category_id):
        statement = f'DELETE FROM category WHERE idCategory == {category_id}'
        self.service.delete(statement)
        flask.flash('Usunięto')
        return flask.redirect(flask.url_for('category.categories_view'))
