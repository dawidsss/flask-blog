import flask

from blog.apps.category import views

blueprint = flask.Blueprint('category', __name__)
blueprint.add_url_rule('', view_func=views.CategoriesView.as_view('categories_view'))
blueprint.add_url_rule('/<category_id>', view_func=views.CategoryView.as_view('category_view'))
blueprint.add_url_rule('/new', view_func=views.CreateCategoryView.as_view('new_view'))
blueprint.add_url_rule('/<category_id>/edit', view_func=views.EditCategoryView.as_view('edit_view'))
blueprint.add_url_rule('/<category_id>/delete', view_func=views.DeletePostView.as_view('delete_view'))
