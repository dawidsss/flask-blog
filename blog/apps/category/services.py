from blog.database import services


class CategoryService(services.Service):
    def get_latest_posts(self, category_id, count=5):
        return self.select((f'SELECT * FROM post '
                            f'WHERE IdCategory == {category_id} '
                            f'ORDER BY Date desc limit {count}'))

    def get_category_name(self, category_id):
        category = self.get_one('category', {'idCategory': category_id})
        return category['name']
