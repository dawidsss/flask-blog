import flask

from blog.apps.blog import views

blueprint = flask.Blueprint('blog', __name__)
blueprint.add_url_rule('/', view_func=views.IndexView.as_view('index_view'))
