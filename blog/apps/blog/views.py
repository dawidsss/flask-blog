import flask

from blog.apps.post import services
from blog.utils import views


class IndexView(views.BaseView):
    service = services.PostService()

    def get(self):
        posts = self.service.get_latest_posts(6)
        return flask.render_template('index.html', posts=posts)
