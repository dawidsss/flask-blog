import flask

from blog.apps.tag import services
from blog.utils import views


class TagView(views.BaseView):
    service = services.TagService()

    def get(self, tag_id):
        posts = self.service.get_latest_posts(tag_id)
        tag_name = self.service.get_tag_name(tag_id)
        if tag_name:
            return flask.render_template('tag.html', posts=posts, tag_name=tag_name)
        flask.flash('Nie ma takiego tagu')
        return flask.redirect(flask.url_for('blog.index_view'))
