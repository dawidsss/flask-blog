from blog.database import services


class TagService(services.Service):
    def get_tags(self, post_id):
        return self.select((
            f'SELECT name, t.idTag FROM tag t, posttag pt '
            f'WHERE t.IdTag = pt.IdTag AND pt.IdPost = {post_id}'
        ))

    def new_tag(self, name):
        return self.put(f'INSERT INTO tag (name) VALUES (\"{name}\")')

    def get_latest_posts(self, tag_id, count=5):
        return self.select((f'SELECT * FROM post p, posttag pt '
                            f'WHERE pt.idTag  == {tag_id} AND p.idPost == pt.idPost '
                            f'ORDER BY Date desc limit {count}'))

    def get_tag_name(self, tag_id):
        tag = self.get_one('tag', {'idTag': tag_id})
        return tag['name']
