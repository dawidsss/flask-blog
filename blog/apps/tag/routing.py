import flask

from blog.apps.tag import views

blueprint = flask.Blueprint('tag', __name__)
blueprint.add_url_rule('/<tag_id>', view_func=views.TagView.as_view('tag_view'))
