import flask

from blog.apps.auth import views

blueprint = flask.Blueprint('auth', __name__)
blueprint.add_url_rule('/login', view_func=views.LoginView.as_view('login_view'))
blueprint.add_url_rule('/register', view_func=views.RegisterView.as_view('register_view'))
blueprint.add_url_rule('/logout', view_func=views.LogoutView.as_view('logout_view'))
