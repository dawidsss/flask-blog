import re

import flask
from werkzeug import security

from blog.apps.auth import services
from blog.utils import views, validation


class LoginView(views.BaseView):
    service = services.LoginService()

    def post(self):
        data = self.get_form()
        user = self.service.get_one('user', {'email': data['email']})

        message = None

        if 'email' not in data:
            message = "Email wymagany!"
        elif 'password' not in data:
            message = "Hasło wymagane"
        elif not validation.is_email(data['email']):
            message = "Niepoprawny format email"
        elif user is None:
            message = 'Nieprawidłowy email lub hasło'
        elif not security.check_password_hash(user['password'], data['password']):
            message = 'Nieprawidłowy email lub hasło'

        if message is None:
            flask.session.clear()
            flask.session['user_id'] = user['idUser']
            flask.session['username'] = user['name']
            flask.session['role_id'] = user['idRole']
            message = 'Zalogowano'

        flask.flash(message)
        return flask.redirect(flask.url_for('blog.index_view'))


class RegisterView(views.BaseView):
    service = services.RegisterService()

    def get(self):
        return flask.render_template('register.html')

    def post(self):
        data = self.get_form().to_dict()
        error = None

        if not data['email']:
            error = 'Email jest wymagany!'
        elif not data['name']:
            error = 'Nick jest wymagany!'
        elif not data['password']:
            error = 'Hasło jest wymagane!'
        elif self.service.get_user(data) is not None:
            error = 'Użytkownik już istnieje'

        if error is None:
            data['password'] = security.generate_password_hash(data['password'])
            if self.service.get_all('user'):
                data['idRole'] = 2
            else:
                data['idRole'] = 1
            self.service.insert('user', data)
            flask.flash('Pomyślnie zarejestrowano')
            return flask.redirect('')
        else:
            flask.flash(error)
            return flask.redirect(flask.url_for('.register_view'))


class LogoutView(views.BaseView):
    def post(self):
        flask.session.clear()
        flask.flash('Wylogowano')
        return flask.redirect(flask.url_for('blog.index_view'))
