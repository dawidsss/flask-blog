import flask

from blog.apps.user import views

blueprint = flask.Blueprint('user', __name__)
blueprint.add_url_rule('', view_func=views.UserView.as_view('user_view'))
blueprint.add_url_rule('/edit', view_func=views.EditUserView.as_view('edit_view'))
blueprint.add_url_rule('/changepassword', view_func=views.ChangePasswordView.as_view('change_password_view'))
