from blog.database import services


class UserService(services.Service):
    def get_latest_posts(self, user_id, count=5):
        return self.select((f'SELECT * FROM post '
                            f'WHERE idUser == {user_id} '
                            f'ORDER BY Date desc limit {count}'))
