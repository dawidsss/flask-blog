import flask
from werkzeug import security

from blog.apps.user import services
from blog.utils import views, decorators


class UserView(views.BaseView):
    service = services.UserService()

    @decorators.user_required
    def get(self):
        data = self.get_query()
        if 'idUser' in data:
            user = self.service.get_one('user', {'idUser': data['idUser']})
        else:
            user = self.service.get_one('user', {'idUser': flask.g.user['user_id']})

        if not user:
            flask.flash('Użytkonik nie istnieje')
            return flask.redirect(flask.url_for('blog.index_view'))

        posts = self.service.get_latest_posts(user['idUser'])

        return flask.render_template('user.html', user=user, posts=posts)


class EditUserView(views.BaseView):
    service = services.UserService()

    @decorators.user_required
    def get(self):
        user = self.service.get_one('user', {'idUser': flask.g.user['user_id']})
        return flask.render_template('editUser.html', user=user)

    @decorators.user_required
    def post(self):
        data = self.get_form().to_dict()
        error = None

        if not data['email']:
            error = 'Email jest wymagany!'
        elif not data['name']:
            error = 'Nick jest wymagany!'

        if error is None:
            statement = (f'UPDATE user '
                         f'SET name = \"{data["name"]}\", email = \"{data["email"]}\" '
                         f'WHERE idUser == {flask.g.user["user_id"]}')
            self.service.put(statement)
            error = 'Zmieniono'

        flask.flash(error)
        return flask.redirect(flask.url_for('user.user_view'))


class ChangePasswordView(views.BaseView):
    service = services.UserService()

    @decorators.user_required
    def get(self):
        user = self.service.get_one('user', {'idUser': flask.g.user['user_id']})
        flask.render_template('changePassword.html', user=user)

    @decorators.user_required
    def post(self):
        data = self.get_form()
        user = self.service.get_one('user', {'idUser': flask.g.user['user_id']})
        error = None

        if 'password' not in data:
            error = "Nowe Hasło wymagane"
        elif 'old_password' not in data:
            error = "Stare Hasło wymagane"
        elif not security.check_password_hash(user['password'], data['old_password']):
            error = 'Nieprawidłowy email lub hasło'

        if error is None:
            new_password = security.generate_password_hash(data['password'])
            statement = (f'UPDATE user '
                         f'SET password = \"{new_password}\" '
                         f'WHERE idUser == {flask.g.user["user_id"]}')
            self.service.put(statement)
            error = 'Zmieniono'

        flask.flash(error)
        return flask.redirect(flask.url_for('user.user_view'))
