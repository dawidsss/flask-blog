import sqlite3


class Service:
    conn = None

    def get_db(self):
        self.conn = sqlite3.connect('blog/database/database.db', detect_types=sqlite3.PARSE_DECLTYPES)
        self.conn.row_factory = sqlite3.Row

    def init_db(self):
        with open('blog/database/database_create.sql') as f:
            sql = f.read()
        self.get_db()
        self.conn.executescript(sql)
        self.conn.commit()
        self.conn.close()

    def get_one(self, tables, data):
        conditions = [f'{key} = :{key}' for key in data]
        statement = f'SELECT * FROM {tables} WHERE {"and ".join(conditions)}'
        self.get_db()
        entity = self.conn.execute(statement, data).fetchone()
        self.conn.close()
        return entity

    def get_list(self, tables, data):
        conditions = [f'{key} = :{key}' for key in data]
        statement = f'SELECT * FROM {tables} WHERE {"and ".join(conditions)}'
        self.get_db()
        entities = self.conn.execute(statement, data).fetchall()
        self.conn.close()
        return entities

    def get_all(self, table):
        statement = f'SELECT * FROM {table}'
        self.get_db()
        entities = self.conn.execute(statement).fetchall()
        self.conn.close()
        return entities

    def select(self, statement):
        self.get_db()
        entities = self.conn.execute(statement).fetchall()
        self.conn.close()
        return entities

    def insert(self, tables, data):
        columns = ', '.join(data)
        placeholders = ':' + ', :'.join(data)
        statement = f'INSERT INTO {tables} ({columns}) VALUES ({placeholders})'
        self.get_db()
        cursor = self.conn.cursor()
        cursor.execute(statement, data)
        self.conn.commit()
        last_entity_id = cursor.lastrowid
        cursor.close()
        self.conn.close()
        return last_entity_id

    def put(self, statement):
        self.get_db()
        cursor = self.conn.cursor()
        cursor.execute(statement)
        self.conn.commit()
        last_entity_id = cursor.lastrowid
        cursor.close()
        self.conn.close()
        return last_entity_id

    def delete(self, statement):
        self.get_db()
        self.conn.execute(statement)
        self.conn.commit()
        self.conn.close()
