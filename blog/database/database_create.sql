-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2018-12-30 13:35:10.054

-- tables
-- Table: Category
CREATE TABLE Category (
    IdCategory integer CONSTRAINT Category_pk PRIMARY KEY AUTOINCREMENT,
    Name varchar(30) NOT NULL
);

-- Table: Comment
CREATE TABLE Comment (
    IdComment integer CONSTRAINT Comment_pk PRIMARY KEY AUTOINCREMENT,
    Content text NOT NULL,
    IdUser integer NOT NULL,
    IdPost integer NOT NULL,
    Date datetime NOT NULL,
    CONSTRAINT Comment_User FOREIGN KEY (IdUser)
    REFERENCES User (IdUser),
    CONSTRAINT Comment_Post FOREIGN KEY (IdPost)
    REFERENCES Post (IdPost)
);

-- Table: Post
CREATE TABLE Post (
    IdPost integer CONSTRAINT Post_pk PRIMARY KEY AUTOINCREMENT,
    Title varchar(64) NOT NULL,
    Content text NOT NULL,
    IdUser integer NOT NULL,
    IdCategory integer NOT NULL,
    Date datetime NOT NULL,
    CONSTRAINT Post_Category FOREIGN KEY (IdCategory)
    REFERENCES Category (IdCategory),
    CONSTRAINT Post_User FOREIGN KEY (IdUser)
    REFERENCES User (IdUser)
);

-- Table: PostTag
CREATE TABLE PostTag (
    IdPost integer NOT NULL,
    IdTag integer NOT NULL,
    CONSTRAINT PostTag_pk PRIMARY KEY (IdPost,IdTag),
    CONSTRAINT PostTag_Post FOREIGN KEY (IdPost)
    REFERENCES Post (IdPost),
    CONSTRAINT PostTag_Tag FOREIGN KEY (IdTag)
    REFERENCES Tag (IdTag)
);

-- Table: Role
CREATE TABLE Role (
    IdRole integer CONSTRAINT Role_pk PRIMARY KEY AUTOINCREMENT,
    Name varchar(30) NOT NULL
);

-- Table: Tag
CREATE TABLE Tag (
    IdTag integer CONSTRAINT Tag_pk PRIMARY KEY AUTOINCREMENT,
    Name integer NOT NULL
);

-- Table: User
CREATE TABLE User (
    IdUser integer CONSTRAINT User_pk PRIMARY KEY AUTOINCREMENT,
    Name varchar(30) NOT NULL,
    Email varchar(30) NOT NULL,
    Password varchar(64) NOT NULL,
    IdRole integer NOT NULL,
    CONSTRAINT User_Role FOREIGN KEY (IdRole)
    REFERENCES Role (IdRole)
);

INSERT INTO Role (name) VALUES ("Administrator");
INSERT INTO Role (name) VALUES ("Użytkownik");

-- End of file.

