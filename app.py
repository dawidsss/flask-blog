import os

import flask

from blog.database import services
from blog.utils import middleware


def register_blueprints(app):
    from blog.apps.post import routing as post_routing
    app.register_blueprint(post_routing.blueprint, url_prefix='/post')
    from blog.apps.auth import routing as auth_routing
    app.register_blueprint(auth_routing.blueprint, url_prefix='/auth')
    from blog.apps.blog import routing as blog_routing
    app.register_blueprint(blog_routing.blueprint, url_prefix='')
    from blog.apps.category import routing as categort_routing
    app.register_blueprint(categort_routing.blueprint, url_prefix='/category')
    from blog.apps.user import routing as user_routing
    app.register_blueprint(user_routing.blueprint, url_prefix='/user')
    from blog.apps.tag import routing as tag_routing
    app.register_blueprint(tag_routing.blueprint, url_prefix='/tag')


def create_app():
    app = flask.Flask(__name__)
    app.config.from_mapping(
        SECRET_KEY=os.environ['Secret_Key'],
    )

    if not os.path.isfile('blog/database/database.db'):
        app.logger.info('initializing')
        database_service = services.Service()
        database_service.init_db()

    app.before_request(middleware.get_session)
    app.before_request(middleware.get_categories)
    register_blueprints(app)

    return app


if __name__ == '__main__':
    create_app().run()
